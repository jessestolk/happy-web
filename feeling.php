<?php
session_start();
require_once 'logindb.php';

if (!isset($_SESSION['username'])) {                                    //als je ingelogd bent doorsturen naar dashboard
    header("Location: login.php");
    exit;
}

?>

<!DOCTYPE html>
<html lang="en" id="feeling">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, minimal-ui">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="./css/main.css">
    <link rel="stylesheet" href="./css/home.css">
    <title>Document</title>
</head>

<body>
    <div id="header">
        <div id="header-blob">
            <blob></blob>
        </div>
    </div>
    <div id="header-pf">
            <?php echo "" . substr($_SESSION['firstname'], 0, 1) . substr($_SESSION['lastname'], 0, 1); ?>
    </div>
    <div id="content">
        <h1 class="overview">How are you feeling today?</h1>
        <div id="feeling-buttons-wrapper">
        <img id="moodmeter" src="./img/MoodMeter.svg" alt="">
        <div id="feeling-buttons">
            <form action="feeling_submit.php" method="POST">
                <button type="submit" class="happyButton" name="1"><img id="afeeling-button" src="./img/Happy1.svg" alt=""></button>
                <button type="submit" class="happyButton" name="2"><img id="faeeling-button" src="./img/Happy2.svg" alt=""></button>
                <button type="submit" class="happyButton" name="3"><img id="feaeling-button" src="./img/Happy3.svg" alt=""></button>
                <button type="submit" class="happyButton" name="4"><img id="feealing-button" src="./img/Happy4.svg" alt=""></button>
                <button type="submit" class="happyButton" name="5"><img id="feelaing-button" src="./img/Happy5.svg" alt=""></button>
            </form>
        </div>
  
        </div>
        
    </div>
</body>

</html>