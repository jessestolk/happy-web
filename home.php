<?php
session_start();
require_once 'logindb.php';

if (!isset($_SESSION['username'])) {                                    //als je ingelogd bent doorsturen naar dashboard
    header("Location: login.php");
    exit;
}

?>

<!DOCTYPE html>
<html id="home" lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, minimal-ui, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="./css/main.css">
    <title>Document</title>
</head>

<body>
    <div id="header">
        <div id="header-blob">
            <a href="logout.php"><blob></blob></a>
        </div>
        <div id="header-pf">
            <?php echo "" . substr($_SESSION['firstname'], 0, 1) . substr($_SESSION['lastname'], 0, 1); ?>
        </div>
    </div>
    <div id="content">
        <h1 class="overview">Overview</h1>
        <a href="feeling.php">
        <div id="task-complete">
            Hello, <?php echo "<b>" . $_SESSION['firstname'] . "</b>!"; ?> <br>
            You have already filled it in today!
        </div>
        </a>
        <div id="button-layout">
            <h2>Options</h2>
            <div id="buttons">
            <div id="option-button"><button id="clickable-tile" class="conversation" onclick="location.href='/happy-web/help.php';"></button></div>
            <div id="option-button"><button id="clickable-tile" class="feeling" onclick="location.href='/happy-web/feelinglog.php';"></button></div>
            <div id="option-button"><button id="clickable-tile" class="none"></button></div>
            <div id="option-button"><button id="clickable-tile" class="none"></button></div>
            </div>
        </div>
    </div>
    <div id="appointments">
        <h2>Appointments</h2>
    </div>
</body>

</html>