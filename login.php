<?php
session_start();
require_once 'logindb.php';

if (isset($_SESSION['username'])) {                                
    header("Location: home.php");
    exit;
}

if (isset($_POST['submit'])) {                                 
    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM `users` WHERE `username` = '$username'";

    $result = $conn->query($sql);                                 
    if ($result->num_rows > 0) {                                   
        while ($row = $result->fetch_assoc()) {                    
            
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, minimal-ui, user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="manifest" href="/site.webmanifest" />
    <link rel="stylesheet" href="./css/main.css">
    <title>Happy!</title>
</head>

<body>
    <div id="splashscreen">
      
    <form action="login.php" method="post" id="login-form">
    
        <label for="username"><b>Username</b></label>
        <input class="input-field" type="text" placeholder=" enter username" name="username" id="username" required> <br>

        <label for="password"><b>Password</b></label>
        <input class="input-field" type="password" placeholder=" enter password" name="password" id="password" required>

        <input type="submit" id="login-button" class="input" name="submit" value="log-in"/>
        <?php
            if (isset($_POST['submit'])) {                                        
                $username = $_POST['username'];                                      
                $password = $_POST['password'];

                $sql = "SELECT * FROM `users` WHERE `username` = '$username'";     
                $result = $conn->query($sql);

                if (empty($username)) {                                              
                    header("location: login.php");                         
                } elseif (empty($password)) {                               
                    header("location: login.php");                      
                } else {
                    if ($result->num_rows > 0) {   
                        while ($row = $result->fetch_assoc()) {                    
                            if ($_POST['password'] == $row['password']) {    

                                $_SESSION['username'] = $username;    
                                $_SESSION['id'] = $row['id'];
                                $pieces = preg_split('/(?=[A-Z])/',$username);      
                                $_SESSION['firstname']   = $pieces[1];       
                                $_SESSION['lastname']   = $pieces[2];    
                                header("location: home.php");                 
                            } else {                                                   
                                
                                header("location: login.php");                
                            }                            
                        }                       
                        
                    }

                }
            }
            ?>
    </form>

      <div id="splash-blobs">
        <svg class="blob-1" viewBox="0 0 127.27 118.22" xmlns="http://www.w3.org/2000/svg">
          <path d="M120.72,24.9c10.8,17.2,7.6,44.1-4.9,63.6-12.4,19.4-34.1,31.5-52.2,29.5-18.2-2-32.9-18.1-45.5-37.6C5.52,60.8-4.88,37.8,2.42,22.6,9.72,7.3,34.92-.1,60.02,0,85.02,0,109.92,7.7,120.72,24.9Z"/>
        </svg>

        <svg class="blob-2" viewBox="0 0 111.77 99.31" xmlns="http://www.w3.org/2000/svg">
          <path d="M90.03,37.1c13.6,20.3,26.8,44.1,19.8,54.7-7.1,10.5-34.3,7.7-57.1,5.1-22.9-2.6-41.2-4.9-48.7-15.7C-3.37,70.5,.03,51.3,9.23,33.6,18.43,15.9,33.33-.3,47.83,0c14.5,.2,28.6,16.9,42.2,37.1Z"/>
        </svg>        

        <svg class="blob-3" viewBox="0 0 118.4 103.94" xmlns="http://www.w3.org/2000/svg">
          <path d="M108.31,13.83c12.9,14.8,13.6,44.1,1.1,63.5-12.5,19.4-38.2,29-58.9,26.1-20.7-2.9-36.5-18.2-44.4-35C-1.89,51.63-2.09,33.33,5.81,21.33,13.61,9.43,29.61,3.83,50.01,1.23c20.5-2.6,45.4-2.2,58.3,12.6Z"/>
        </svg>
      </div>

      <svg id="bg-shape" xmlns="http://www.w3.org/2000/svg" viewBox="00 0 1080 999">
        <path class="shape-3" d="M0,1018.13H1080V159.29C945.12,61.27,753.09,0,540,0S134.88,61.27,0,159.29V1018.13Z"></path>
        <path class="shape-2" d="M0,1018.13H1080V380.14c-134.88-98.02-326.91-159.29-540-159.29S134.88,282.12,0,380.14V1018.13Z"></path>
        <path class="shape-1" d="M0,1018.13H1080v-427.13c-134.88-98.02-326.91-159.29-540-159.29S134.88,492.97,0,591v427.13Z"></path>
    </svg>

    </div> 
</body>
</html>