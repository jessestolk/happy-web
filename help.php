<?php
session_start();
require_once 'logindb.php';

if (!isset($_SESSION['username'])) {                                    //als je ingelogd bent doorsturen naar dashboard
    header("Location: login.php");
    exit;
}

?>

<!DOCTYPE html>
<html id="home" lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, minimal-ui, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="./css/main.css">
    <title>Document</title>
</head>

<body id="help">
    <div id="header">
        <div id="header-blob">
            <blob></blob>
        </div>
        <div id="header-pf">
            <?php echo "" . substr($_SESSION['firstname'], 0, 1) . substr($_SESSION['lastname'], 0, 1); ?>
        </div>
    </div>
    <div id="content">
        <h1 class="overview">Helpful info</h1>
        <div id="greeting">
            Hello <?php echo $_SESSION['firstname']; ?>, <br>
            Do you have a question, or want some help? Please take a look here.
        </div>
        <div class="item">
            <h3><a href="https://www.thuisarts.nl/psychische-klachten/ik-heb-psychische-klachten">Mental<a></h3>
            <p>Thuisarts</p>
        </div>
        <div class="item">
            <h3><a href="https://www.stoppestennu.nl/ik-zoek-hulp-ik-word-gepest-bij-wie-kan-ik-terecht">Bullying<a></h3>
            <p>Stop Pesten Nu</p>
        </div>
        <div class="item">
            <h3><a href="https://www.113.nl/">Suicide<a></h3>
            <p>113.nl</p>
        </div>
        <div class="item">
            <h3><a href="https://www.deluisterlijn.nl">Loneliness<a></h3>
            <p>De Luisterlijn</p>
        </div>
        <div class="item">
            <h3><a href="https://pratenoververlies.nl">Grieve<a></h3>
            <p>Praten over verlies</p>
        </div>
</body>

</html>