<?php
session_start();
require_once 'logindb.php';

// if (!isset($_SESSION['username'])) {                                    //als je ingelogd bent doorsturen naar dashboard
//     header("Location: login.php");
//     exit;
// }

$id = $_SESSION['id'];
$sql = "SELECT * FROM `feelings` WHERE `userid` = $id";

    
?>

<!DOCTYPE html>
<html id="home" lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, minimal-ui">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="./css/main.css">
    <title>Document</title>
</head>

<body id="feelinglog">
    <div id="header">
        <div id="header-blob">
            <blob></blob>
        </div>
        <div id="header-pf">
            <?php echo "" . substr($_SESSION['firstname'], 0, 1) . substr($_SESSION['lastname'], 0, 1); ?>
        </div>
    </div>
    <div id="content">
        <h1 class="overview">Personal Logbook</h1>
        <div id="greeting">
            Feeling Log of <?php echo $_SESSION['firstname']; ?>, <br>
        </div>
        <table>
            <thead>
                <tr>
                    <th >Feeling</th>
                    <th >Comment</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $result = $conn->query($sql);                                 
                    if ($result->num_rows > 0) {                                   
                        while ($row = $result->fetch_assoc()) {                    
                            echo "<tr>";
                            echo "<th><img src='./img/Happy" . $row["feeling"] . ".svg' alt='' width='40px'></th>";
                            echo "<td>" . $row["comment"] . "</td>";
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
</body>

</html>